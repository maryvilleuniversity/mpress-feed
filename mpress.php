<?php
/*
Plugin Name: Mpress Feed
Plugin URI: http://maryville.edu/
Description: Displays a feed for the Mpress Blog onto other sites
Version: 1.0
Author: Alex Howell
Author URI: blogs.maryville.edu/ahowell1
*/

function mpress_feed_function($atts) {

	extract( shortcode_atts( array(
        //'parent' => 8,
        'tag' => '',
        'type' => 'post',
        'perpage' => 3,
        'large' => '3',
        'medium' => '2',
        'small' => '1'
    ), $atts ) );
 global $switched;
 $oldBlog = get_current_blog_id();
 $blog_id = 64;

 switch_to_blog( $blog_id );
 $output = '<div class="clear"></div><ul class="feed large-block-grid-'.$large.' medium-block-grid-'.$medium.' small-block-grid-'.$small.'">';
    $args = array(
        //'post_parent' => $parent,
        'tag' => $tag,
        'post_type' => $type,
        'posts_per_page' => $perpage,
        'sort_column'   => 'menu_order'
    );
    $mpress_query = new  WP_Query( $args );
    while ( $mpress_query->have_posts() ) : $mpress_query->the_post();
        $output .= '<li>
                  <a target="_blank" style="bottom:0;" class="read-more" href="'
                   .get_permalink().
                   '">'
                   .get_the_post_thumbnail().
                   '</a>
                   <h5 style="margin-bottom:5px;font-weight: bold;font-style: Open-sans;">
                   <a target="_blank" style="bottom:0;" class="read-more" href="'
                   .get_permalink().
                   '">'
                   .get_the_title().
                   '
                   </a>
                   </h5>
                   '
                   ;
    endwhile;
    wp_reset_query();
    $output .= '</ul>';
    restore_current_blog();
    return $output;
}
add_shortcode('mpress', 'mpress_feed_function');
?>